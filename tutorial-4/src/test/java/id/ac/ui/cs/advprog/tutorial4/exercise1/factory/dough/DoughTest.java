package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class DoughTest {

    private CornmealCrustDough cornmealCrustDough;
    private ThickCrustDough thickCrustDough;
    private ThinCrustDough thinCrustDough;

    @Before
    public void setUp() {

        cornmealCrustDough = new CornmealCrustDough();
        thickCrustDough = new ThickCrustDough();
        thinCrustDough = new ThinCrustDough();
    }

    @Test
    public void testDoughName() {

        assertEquals(cornmealCrustDough.toString(), "Cornmeal Crust so crunchy you will never forget it");
        assertEquals(thickCrustDough.toString(), "ThickCrust style extra thick crust dough");
        assertEquals(thinCrustDough.toString(), "Thin Crust Dough");
    }

}
