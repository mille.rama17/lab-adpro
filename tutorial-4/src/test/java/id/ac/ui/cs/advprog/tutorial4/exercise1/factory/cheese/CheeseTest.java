package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CheeseTest {

    private MeltedCheese meltedCheese;
    private MozzarellaCheese mozzarellaCheese;
    private ParmesanCheese parmesanCheese;
    private ReggianoCheese reggianoCheese;

    @Before
    public void setUp() {

        meltedCheese = new MeltedCheese();
        mozzarellaCheese = new MozzarellaCheese();
        parmesanCheese = new ParmesanCheese();
        reggianoCheese = new ReggianoCheese();

    }

    @Test
    public void testCheeseName() {

        assertEquals(meltedCheese.toString(), "Melted Cheese");
        assertEquals(mozzarellaCheese.toString(), "Shredded Mozzarella");
        assertEquals(parmesanCheese.toString(), "Shredded Parmesan");
        assertEquals(reggianoCheese.toString(), "Reggiano Cheese");
    }

}
