package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SauceTest {

    private MarinaraSauce marinaraSauce;
    private PlumTomatoSauce plumTomatoSauce;
    private SpecialSecretSauce specialSecretSauce;

    @Before
    public void setUp() {

        marinaraSauce = new MarinaraSauce();
        plumTomatoSauce = new PlumTomatoSauce();
        specialSecretSauce = new SpecialSecretSauce();
    }

    @Test
    public void testSauceName() {

        assertEquals(marinaraSauce.toString(), "Marinara Sauce");
        assertEquals(plumTomatoSauce.toString(), "Tomato sauce with plum tomatoes");
        assertEquals(specialSecretSauce.toString(), "a Special Secret sauce guaranteed to blow you away");
    }

}