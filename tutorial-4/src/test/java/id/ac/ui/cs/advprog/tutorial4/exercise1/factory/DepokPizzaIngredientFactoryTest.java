package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MeltedCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.LuxuryClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.CornmealCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.SpecialSecretSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.FriedCabbage;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import java.util.stream.IntStream;
import org.junit.Before;
import org.junit.Test;



public class DepokPizzaIngredientFactoryTest {

    private DepokPizzaIngredientFactory depokPizzaIngredientFactory;

    @Before
    public void setUp() {

        depokPizzaIngredientFactory = new DepokPizzaIngredientFactory();
    }

    @Test
    public void testCreateDough() {

        CornmealCrustDough cornmealCrustDough = new CornmealCrustDough();
        assertEquals(cornmealCrustDough.getClass(),
                depokPizzaIngredientFactory.createDough().getClass());
        assertEquals(cornmealCrustDough.toString(),
                depokPizzaIngredientFactory.createDough().toString());

    }

    @Test
    public void testCreateCheese() {

        MeltedCheese meltedCheese = new MeltedCheese();
        assertEquals(meltedCheese.getClass(),
                depokPizzaIngredientFactory.createCheese().getClass());
        assertEquals(meltedCheese.toString(),
                depokPizzaIngredientFactory.createCheese().toString());

    }

    @Test
    public void testCreateSauce() {

        SpecialSecretSauce specialSecretSauce = new SpecialSecretSauce();
        assertEquals(specialSecretSauce.getClass(),
                depokPizzaIngredientFactory.createSauce().getClass());
        assertEquals(specialSecretSauce.toString(),
                depokPizzaIngredientFactory.createSauce().toString());

    }

    @Test
    public void testCreateClam() {

        LuxuryClams luxuryClams = new LuxuryClams();
        assertEquals(luxuryClams.getClass(), depokPizzaIngredientFactory.createClam().getClass());
        assertEquals(luxuryClams.toString(), depokPizzaIngredientFactory.createClam().toString());
    }

    @Test
    public void testCreateVeggie() {

        Veggies[] veggies = {new BlackOlives(), new Mushroom(), new FriedCabbage(), new Onion()};
        Veggies[] nyPizzaVeggies = depokPizzaIngredientFactory.createVeggies();
        IntStream.range(0, veggies.length).forEach(i -> assertEquals(veggies[i].toString(),
                nyPizzaVeggies[i].toString()));

    }
}
