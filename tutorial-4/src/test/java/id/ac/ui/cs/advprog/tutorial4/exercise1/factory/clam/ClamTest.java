package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ClamTest {

    private FreshClams freshClams;
    private FrozenClams frozenClams;
    private LuxuryClams luxuryClams;

    @Before
    public void setUp() {

        freshClams = new FreshClams();
        frozenClams = new FrozenClams();
        luxuryClams = new LuxuryClams();
    }

    @Test
    public void testClamsName() {

        assertEquals(freshClams.toString(), "Fresh Clams from Long Island Sound");
        assertEquals(frozenClams.toString(), "Frozen Clams from Chesapeake Bay");
        assertEquals(luxuryClams.toString(), "Luxury Clams so expensive you cannot afford it");
    }


}
