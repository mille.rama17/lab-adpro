package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;


public class OrderingPizzaTest {
    private NewYorkPizzaStore nyStore;
    private DepokPizzaStore depokStore;

    @Before
    public void setUp() throws Exception {

        nyStore = new NewYorkPizzaStore();
        depokStore = new DepokPizzaStore();
    }

    @Test
    public void testOrderingCheesePizza() {

        Pizza nyPizza = nyStore.createPizza("cheese");
        assertEquals("New York Style Cheese Pizza", nyPizza.getName());

        Pizza depokPizza = depokStore.createPizza("cheese");
        assertEquals("Depok Style Cheese Pizza", depokPizza.getName());

        Pizza orderNyPizza = nyStore.orderPizza("cheese");
        assertEquals("---- New York Style Cheese Pizza ----\n" +
                "Thin Crust Dough\n" +
                "Marinara Sauce\n" +
                "Reggiano Cheese\n", orderNyPizza.toString());

        Pizza orderDepokPizza = depokStore.orderPizza("cheese");
        assertEquals("---- Depok Style Cheese Pizza ----\n" +
                "Cornmeal Crust so crunchy you will never forget it\n" +
                "a Special Secret sauce guaranteed to blow you away\n" +
                "Melted Cheese\n", orderDepokPizza.toString());
    }


    @Test
    public void testOrderingClamPizza() {

        Pizza nyPizza = nyStore.createPizza("clam");
        assertEquals("New York Style Clam Pizza", nyPizza.getName());

        Pizza depokPizza = depokStore.createPizza("clam");
        assertEquals("Depok Style Clam Pizza", depokPizza.getName());

        Pizza orderNyPizza = nyStore.orderPizza("clam");
        assertEquals("---- New York Style Clam Pizza ----\n" +
                "Thin Crust Dough\n" +
                "Marinara Sauce\n" +
                "Reggiano Cheese\n" +
                "Fresh Clams from Long Island Sound\n", orderNyPizza.toString());

        Pizza orderDepokPizza = depokStore.orderPizza("clam");
        assertEquals("---- Depok Style Clam Pizza ----\n" +
                "Cornmeal Crust so crunchy you will never forget it\n" +
                "a Special Secret sauce guaranteed to blow you away\n" +
                "Melted Cheese\n" +
                "Luxury Clams so expensive you cannot afford it\n", orderDepokPizza.toString());
    }

    @Test
    public void testOrderingVeggiePizza() {

        Pizza nyPizza = nyStore.createPizza("veggie");
        assertEquals("New York Style Veggie Pizza", nyPizza.getName());

        Pizza depokPizza = depokStore.createPizza("veggie");
        assertEquals("Depok Style Veggie Pizza", depokPizza.getName());

        Pizza orderNyPizza = nyStore.orderPizza("veggie");
        assertEquals("---- New York Style Veggie Pizza ----\n" +
                "Thin Crust Dough\n" +
                "Marinara Sauce\n" +
                "Reggiano Cheese\n" +
                "Garlic, Onion, Mushrooms, Red Pepper\n", orderNyPizza.toString());

        Pizza orderDepokPizza = depokStore.orderPizza("veggie");
        assertEquals("---- Depok Style Veggie Pizza ----\n" +
                "Cornmeal Crust so crunchy you will never forget it\n" +
                "a Special Secret sauce guaranteed to blow you away\n" +
                "Melted Cheese\n" +
                "Black Olives, Mushrooms, Doesn't belong here, goes well with " +
                "fried chicken though, Onion\n", orderDepokPizza.toString());

    }


}
