package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;


public class VeggiesTest {

    private BlackOlives blackOlives;
    private Eggplant eggplant;
    private FriedCabbage friedCabbage;
    private Garlic garlic;
    private Mushroom mushroom;
    private Onion onion;
    private RedPepper redPepper;
    private Spinach spinach;

    @Before
    public void setUp() {

        blackOlives = new BlackOlives();
        eggplant = new Eggplant();
        friedCabbage = new FriedCabbage();
        garlic = new Garlic();
        mushroom = new Mushroom();
        onion = new Onion();
        redPepper = new RedPepper();
        spinach = new Spinach();
    }

    @Test
    public void testVeggiesName() {

        assertEquals(blackOlives.toString(), "Black Olives");
        assertEquals(eggplant.toString(), "Eggplant");
        assertEquals(friedCabbage.toString(), "Doesn't belong here, goes well with fried chicken though");
        assertEquals(garlic.toString(), "Garlic");
        assertEquals(mushroom.toString(), "Mushrooms");
        assertEquals(onion.toString(), "Onion");
        assertEquals(redPepper.toString(), "Red Pepper");
        assertEquals(spinach.toString(), "Spinach");

    }

}
