package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class LuxuryClams implements Clams {

    public String toString() {
        return "Luxury Clams so expensive you cannot afford it";
    }
}
