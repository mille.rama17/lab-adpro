package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class SpecialSecretSauce implements Sauce {
    public String toString() {
        return "a Special Secret sauce guaranteed to blow you away";
    }
}
