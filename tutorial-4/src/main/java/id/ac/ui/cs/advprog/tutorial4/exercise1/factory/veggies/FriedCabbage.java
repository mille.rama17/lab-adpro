package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class FriedCabbage implements Veggies {
    public String toString() {
        return "Doesn't belong here, goes well with fried chicken though";
    }
}
