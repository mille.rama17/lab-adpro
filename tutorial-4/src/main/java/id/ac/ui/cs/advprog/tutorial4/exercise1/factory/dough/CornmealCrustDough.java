package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class CornmealCrustDough implements Dough {

    public String toString() {
        return "Cornmeal Crust so crunchy you will never forget it";
    }
}

