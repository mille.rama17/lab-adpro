package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CurriculumVitaeController {

    @GetMapping("/mycv")
    public String mycv(@RequestParam(name = "name", required = false)
                                   String name, Model model) {
        if (name == null) {
            model.addAttribute("name", "This is my CV");
        } else if (name.equals("")) {
            model.addAttribute("name", "This is my CV");
        } else {
            model.addAttribute("name", name + ", I hope you interested to hire me");
        }
        return "mycv";
    }

}
