package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class DecoratorMain {
    public static void main(String[] args) {
        Food krabbyPatty;

        krabbyPatty = BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        System.out.println(krabbyPatty.getDescription());
        System.out.println(krabbyPatty.cost());
        krabbyPatty = FillingDecorator.BEEF_MEAT.addFillingToBread(krabbyPatty);
        System.out.println(krabbyPatty.getDescription());
        System.out.println(krabbyPatty.cost());
        krabbyPatty = FillingDecorator.CHEESE.addFillingToBread(krabbyPatty);
        System.out.println(krabbyPatty.getDescription());
        System.out.println(krabbyPatty.cost());
        krabbyPatty = FillingDecorator.LETTUCE.addFillingToBread(krabbyPatty);
        System.out.println(krabbyPatty.getDescription());
        System.out.println(krabbyPatty.cost());
        krabbyPatty = FillingDecorator.TOMATO.addFillingToBread(krabbyPatty);
        System.out.println(krabbyPatty.getDescription());
        System.out.println(krabbyPatty.cost());
        krabbyPatty = FillingDecorator.TOMATO_SAUCE.addFillingToBread(krabbyPatty);
        System.out.println(krabbyPatty.getDescription());
        System.out.println(krabbyPatty.cost());
        krabbyPatty = FillingDecorator.CUCUMBER.addFillingToBread(krabbyPatty);
        System.out.println(krabbyPatty.getDescription());
        System.out.println(krabbyPatty.cost());
        krabbyPatty = FillingDecorator.CHILI_SAUCE.addFillingToBread(krabbyPatty);
        System.out.println(krabbyPatty.getDescription());
        System.out.println(krabbyPatty.cost());
        krabbyPatty = FillingDecorator.CHICKEN_MEAT.addFillingToBread(krabbyPatty);
        System.out.println(krabbyPatty.getDescription());
        System.out.println(krabbyPatty.cost());

    }
}
