package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class CompositeMain {
    public static void main(String[] args) {
        Company universe7 = new Company();

        Ceo gohan;

        gohan = new Ceo("Gohan", 1000000000);
        universe7.addEmployee(gohan);

        Cto goku;

        goku = new Cto("Goku", 750000000);
        universe7.addEmployee(goku);

        BackendProgrammer vegeta;

        vegeta = new BackendProgrammer("Vegeta", 500000000);
        universe7.addEmployee(vegeta);

        FrontendProgrammer frieza;

        frieza = new FrontendProgrammer("Frieza", 495000000);
        universe7.addEmployee(frieza);

        UiUxDesigner roshi;

        roshi = new UiUxDesigner("Master Roshi", 400000000);
        universe7.addEmployee(roshi);

        SecurityExpert android17;

        android17 = new SecurityExpert("Android 17", 497000000);
        universe7.addEmployee(android17);

        NetworkExpert piccolo;

        piccolo = new NetworkExpert("Piccolo", 450000000);
        universe7.addEmployee(piccolo);

        for (Employees fighter : universe7.getAllEmployees()) {
            System.out.println(fighter.getName());
            System.out.println(fighter.getRole());
            System.out.println(fighter.getSalary());
        }
        System.out.println(universe7.getNetSalaries());

    }

}
