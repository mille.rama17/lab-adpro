package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class BackendProgrammer extends Employees {

    public BackendProgrammer(String name, double salary) {
        this.name = name;
        if (salary < 20000000.00) {
            throw new IllegalArgumentException();
        } else {
            this.salary = salary;
        }
        this.role = "Back End Programmer";
    }

    @Override
    public double getSalary() {
        return salary;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }
}
